const db = require('./models');
require('./models/users.model');
const controller = require('./controllers/users.controller');

const main = async () => {
	try {
		await db.sync({ force: true }).then(() => {
			console.log('Drop and re-sync db.');
		});
		controller.create({
			name: 'Shaik',
			age: 23,
			email: 'shaik@gmail.com',
		});
	} catch (err) {
		console.log(err);
	}
};

main();

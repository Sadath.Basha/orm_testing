const Sequelize = require('sequelize');
const connection = require('./index');

const Users = connection.define(
	'users',
	{
		name: {
			type: Sequelize.STRING,
		},
		age: {
			type: Sequelize.INTEGER,
		},
		email: {
			type: Sequelize.STRING,
		},
	},
	{
		freezeTableName: true,
	}
);

module.exports = Users;

const Sequelize = require('sequelize');
const Users = require('../models/users.model');

const { Op } = Sequelize;

const create = async (userObj) => {
	try {
		if (!userObj.name) {
			console.log('Content can not be empty!');
		}

		const insertData = await Users.create(userObj);
		console.log(insertData.dataValues);
	} catch (err) {
		console.log(err);
	}
};

const findAll = async (name) => {
	try {
		const condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

		const selectAllData = await Users.findAll({ where: condition });
		console.log(selectAllData.dataValues);
	} catch (err) {
		console.log(err);
	}
};

const findOne = async (id) => {
	try {
		const selectOneData = await Users.findByPk(id);
		if (selectOneData == null) {
			console.log(`No data available with id ${id}`);
		} else {
			console.log(selectOneData.dataValues);
		}
	} catch (err) {
		console.log(err);
	}
};

const update = async (id, userObj) => {
	try {
		const updatedData = await Users.update(userObj, {
			where: { id },
		});

		if (updatedData == 1) {
			console.log('user was updated successfully');
		} else {
			console.log(
				`Cannot update Users with id=${id}. Maybe Users was not found or req.body is empty!`
			);
		}
	} catch (err) {
		console.log(err);
	}
};

const deleteOne = async (id) => {
	try {
		const deletedData = await Users.destroy({
			where: { id },
		});
		if (deletedData === 1) {
			console.log('User was deleted successfully!');
		} else {
			console.log(`Cannot delete Users with id=${id}. Maybe Users was not found!`);
		}
	} catch (err) {
		console.log(err);
	}
};

module.exports = {
	create,
	findAll,
	findOne,
	update,
	deleteOne,
};
